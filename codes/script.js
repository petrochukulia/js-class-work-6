function Calculator() {

	this.read = function () {
		this.first = parseFloat(prompt("Введіть перше число")),
			this.second = parseFloat(prompt("Введіть друге число"));
		return document.getElementById("read").innerHTML = `Ваше перше число: ${this.first}, ваше друге число: ${this.second} <br>`;
	},

		this.sum = function () {
			let plus = this.first + this.second;
			return document.getElementById("sum").innerHTML = `Сума чисел дорівнює: ${plus} `;
		}

	this.mul = function () {
		let mul = this.first * this.second;
		return document.getElementById("mul").innerHTML = `Множення чисел дорівнює: ${mul} `;
	}

}

let one = new Calculator();
one.read();
one.sum();
one.mul();

